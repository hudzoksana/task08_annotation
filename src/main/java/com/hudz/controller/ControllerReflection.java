package com.hudz.controller;

import com.hudz.model.MyOwnClass;
import com.hudz.model.ReflectionUtil;
import com.hudz.model.TestAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ControllerReflection implements Controller {

  public ControllerReflection() {

  }
  private final Logger logger = LogManager.getLogger(Controller.class);
  private TestAnnotation testAnnotation = new TestAnnotation();
  private ReflectionUtil reflectionUtil = new ReflectionUtil();

  public List<String> getNamesListOfAnnotatedFields() {
    return reflectionUtil.getNamesListOfAnnotatedFields(testAnnotation.getClass());
  }

  public List<String> getAnnotationValues() {
    return reflectionUtil.getAnnotationValues(testAnnotation.getClass());
  }

  public String getStringResultOfInvokeThreeMethods() {
    String result =
            reflectionUtil.getStringResultOfInvokeThreeMethods(testAnnotation);
    if (result == null) {
      logger.info("Error with methods executing");
    }
    return reflectionUtil.getStringResultOfInvokeThreeMethods(testAnnotation);
  }

  public <T> String setFieldValue(String name, T value) {
    reflectionUtil.setValueIntoField(testAnnotation, name, value);
    return testAnnotation.toString();
  }

  public String invokeMyMethods() {
    return reflectionUtil.invokeMyMethods(testAnnotation);
  }

  public <T> String getAllInfo(T obj) {
    MyOwnClass myInfoClass = new MyOwnClass(obj);
    return myInfoClass.getInfo();
  }
}

