package com.hudz.model;

import java.lang.reflect.*;

public class MyOwnClass {

    private Class clazz;

    public <T> MyOwnClass(T obj) {
        clazz = obj.getClass();
    }

    public String getInfo() {
        return "Our class name is: " + clazz.getName() +
                "\nFields:\n" + getFieldsInfo() +
                "\nMethods:\n" + getMethodsInfo() +
                "\nConstructors:\n" + getConstructorsInfo();
    }

    private String getConstructorsInfo() {
        StringBuilder constructorsInfo = new StringBuilder();
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            constructorsInfo
                    .append(" ")
                    .append(Modifier.toString(constructor.getModifiers()))
                    .append(" ")
                    .append(clazz.getSimpleName())
                    .append("(")
                    .append(getParameters(constructor))
                    .append(")")
                    .append(" ; ");
        }
        return constructorsInfo.toString();
    }

    private String getFieldsInfo() {
        StringBuilder fieldsInfo = new StringBuilder();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            fieldsInfo
                    .append(" ")
                    .append(Modifier.toString(field.getModifiers()))
                    .append(" ")
                    .append(field.getType().getSimpleName())
                    .append(" ")
                    .append(field.getName())
                    .append(";");
        }
        return fieldsInfo.toString();
    }

    private String getMethodsInfo() {
        StringBuilder methodsInfo = new StringBuilder();
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            methodsInfo
                    .append(" ")
                    .append(Modifier.toString(method.getModifiers()))
                    .append(" ")
                    .append(method.getReturnType().getSimpleName())
                    .append(" ")
                    .append(method.getName())
                    .append("(")
                    .append(getParameters(method))
                    .append(");");
        }
        return methodsInfo.toString();
    }

    private String getParameters(Executable executable) {
        Class[] parameters = executable.getParameterTypes();
        StringBuilder allParameters = new StringBuilder();
        if (parameters.length > 0) {
            for (int i = 0; i < parameters.length; i++) {
                 if(i < parameters.length - 1) {
                     allParameters.append(parameters[i].getSimpleName()).append(", ");
                 } else {
                     allParameters.append(parameters[i].getSimpleName());
                 }
            }
        }
        return allParameters.toString();
    }
}
