package com.hudz.model;

import com.hudz.Annotation.MyAnnotation;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.IntStream;

public class TestAnnotation {

    private int id;
    @MyAnnotation (name = "settedName")
    private String name;
    @MyAnnotation
    private double age;

    public TestAnnotation(int id, String name, double age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public TestAnnotation(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public TestAnnotation() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public int doMethod1(int number) {
        return number;
    }

    public double doMethod2(String string) {
        return age = 2.0;
    }

    public String doMethod3() {
        return "I`m method #3. Returning this string. Without parameters";
    }

    public String myMethod(String a, int... args) {
        return "String argument: " + a + ", int... args sum(): " + Arrays.stream(args).sum();
    }

    public int myMethod(String... args) {
        return args.length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestAnnotation)) return false;
        TestAnnotation that = (TestAnnotation) o;
        return getId() == that.getId() &&
                Double.compare(that.getAge(), getAge()) == 0 &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getAge());
    }

    @Override
    public String toString() {
        return "TestAnnotation{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
