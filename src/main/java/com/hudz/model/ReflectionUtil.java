package com.hudz.model;

import com.hudz.Annotation.MyAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ReflectionUtil {

    private final Logger logger = LogManager.getLogger(ReflectionUtil.class);

    private List<Field> getFieldsWithMyAnnotation(Class clazz) {
        List<Field> annotatedFields = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                annotatedFields.add(field);
            }
        }
        return annotatedFields;
    }

    public List<String> getNamesListOfAnnotatedFields(Class clazz) {
        List<String> fieldNames = new LinkedList<>();
        List<Field> annotatedFields = getFieldsWithMyAnnotation(clazz);
        annotatedFields.forEach(s -> {
                s.setAccessible(true);
                fieldNames.add(s.getType().getSimpleName() + " " + s.getName());
        });
        return fieldNames;
    }

    public List<String> getAnnotationValues(Class clazz) {
        List<Field> annotatedFields = getFieldsWithMyAnnotation(clazz);
        List<String> annotationsInfo = new LinkedList<>();
        annotatedFields.forEach(s -> {
                    MyAnnotation ann =
                            s.getAnnotation(MyAnnotation.class);
                    annotationsInfo.add("@" + ann.annotationType()
                            .getSimpleName() + "(name = \"" + ann.name() +
                            "\")");
                }
        );
        return annotationsInfo;
    }

    public String getStringResultOfInvokeThreeMethods(TestAnnotation testAnnotation) {
        Method[] method = new Method[3];
        String result = "Empty result";
        try {
            method[0] = TestAnnotation.class.getDeclaredMethod("doMethod1", int.class);
            method[1] = TestAnnotation.class.getDeclaredMethod("doMethod2", String.class);
            method[2] = TestAnnotation.class.getDeclaredMethod("doMethod3");

            double resultMethod2 = (double)method[1].invoke(testAnnotation, "example");
            String resultMethod3 = (String) method[2].invoke(testAnnotation);
            result = "First method result : " + method[0].invoke(testAnnotation, 3) +
                    "\nSecond method: " + resultMethod2 +
                    "\nThird method : " + resultMethod3;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }

    public <T> void setValueIntoField(TestAnnotation obj, String name,
                                      T newValue) {
        try {
            Field someField = TestAnnotation.class.getDeclaredField(name);
            someField.setAccessible(true);
            someField.set(obj, newValue);
        } catch (NoSuchFieldException | IllegalAccessException
                | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public String invokeMyMethods(TestAnnotation testAnnotation)  {
        String resultOfStringAIntArgs = "No result myMethod(String a, int... args)";
        int resultOfStringArgs = 0;
        Method myMethod1 = null;
        try {
            myMethod1 = TestAnnotation.class.getDeclaredMethod(
                        "myMethod", String.class, int[].class);
            Method myMethod2 = TestAnnotation.class.getDeclaredMethod("myMethod", String[].class);
            resultOfStringAIntArgs = (String) myMethod1.invoke(testAnnotation, "string", new int[]{1, 2, 3});
            resultOfStringArgs = (int) myMethod2.invoke(testAnnotation, (Object) new String[]{"first string", "second string", "third string"});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return "String myMethod(String a, int... args) result: " + resultOfStringAIntArgs + "\nint myMethod(String... args) result: " + resultOfStringArgs;
    }
}
