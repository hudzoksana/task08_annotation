package com.hudz;

import com.hudz.view.TasksMenu;

public class Application {

  public static void main(String[] args) throws Exception {
    new TasksMenu().show();
  }
}
