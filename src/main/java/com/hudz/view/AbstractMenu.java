package com.hudz.view;

import com.hudz.controller.ControllerReflection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public abstract class AbstractMenu {
    Locale locale;
    ResourceBundle bundle;
    protected ControllerReflection controller;
    protected Map<String, String> menu;
    protected Map<String, Printable> methodsMenu;
    protected Scanner input;
    protected final Logger logger = LogManager.getLogger(AbstractMenu.class);

    AbstractMenu() {
        controller = new ControllerReflection();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
    }
    protected void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : this.menu.values()) {
            System.out.println(str);
        }
    }
    public void show() {
        String keySubMenu;
        do {
            this.outputMenu();
            System.out.println("Please, select menu point.");
            keySubMenu = input.nextLine().toUpperCase();

            try {
                this.methodsMenu.get(keySubMenu).print();
            } catch (Exception e) {
                System.out.println("not correct");
            }
        } while (!keySubMenu.equals("Q"));
    }


}
