package com.hudz.view;

import com.hudz.model.MyOwnClass;
import com.hudz.model.TestAnnotation;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.ResourceBundle;

public class TasksMenu extends AbstractMenu {

  private void setMenu() {
    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));

    menu.put("Q", bundle.getString("Q"));
  }

  public TasksMenu() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();

    methodsMenu.put("1", this::printAnnotatedFields);
    methodsMenu.put("2", this::printAnnotationValues);
    methodsMenu.put("3", this::printResultThreeMethods);
    methodsMenu.put("4", this::printSettingFieldValue);
    methodsMenu.put("5", this::printResultMyMethods);
    methodsMenu.put("6", this::printInfoAboutClass);
  }

  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  public void printAnnotatedFields() {
    logger.trace("Fields with MyAnnotation:\n" + controller.getNamesListOfAnnotatedFields());
  }

  private void printAnnotationValues() {
    logger.trace("Annotation values of \"name\" " + controller.getAnnotationValues());
  }

  public void printResultThreeMethods() {
    logger.trace("Invoking 3 methods:\n" + controller.getStringResultOfInvokeThreeMethods());
  }

  public void printSettingFieldValue() {

    logger.trace("Input new value for field [name] : ");
    logger.trace (controller.setFieldValue("name", input.next()));
  }

  public void printResultMyMethods() {
    logger.trace(controller.invokeMyMethods());
  }

  public void printInfoAboutClass() {
    logger.trace("Information of the class : ");
    logger.trace(controller.getAllInfo(TestAnnotation.class));
  }
}
