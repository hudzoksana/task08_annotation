package com.hudz.Annotation;

import java.lang.annotation.*;

@Documented
@Inherited
@Target({ElementType.FIELD,ElementType.TYPE,ElementType.METHOD,ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
    String name() default "Name1";
}
